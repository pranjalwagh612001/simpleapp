SimpleApp
created a simple java file which perform print statement and simple calculations.



Add your files
- push an existing Git repository with the following command:


git add . git status
git commit -m 'new files added'
git remote add origin https://gitlab.com/pranjalwagh612001/simpleapp.git 
git push -uf origin main

-	create Jenkins job in pipeline project.
-	check for Trigger builds remotely and provide the jenkins token.
-	choose Pipeline script from SCM.
-	provide your gitlab credentials.
-	save the configuration.

Jenkinsfile

-	file containing stages for checkout, build, test and deploy
-	Checkout: checkout the gitlab repo using credential
-	Build: build the SimpleApp
-	Test: test the SimpleApp
-	Deploy: deploy the SimpleApp


GitLab CI Configuration
-	generate Jenkins API token
-	install Gitlab plugin in Available plugins
-	go to your repository -> settings -> CI/CD -> Variables

Add a variable named JENKINS_URL with the value being the URL of your Jenkins server.
Add a variable named JENKINS_TOKEN with the value being the API token you generated

-	GitLab repository, go to Settings > Integrations
-	add a new webhook
-	Set the URL to http://YOUR_JENKINS_SERVER/project/YOUR_JENKINS_JOB_NAME/build?token=YOUR_JENK INS_TOKEN
-	Choose the events that should trigger the webhook.
 
-	Save the webhook.

.gitlab-ci.yml
-	write a code to trigger the Jenkins pipeline on each commit to the repository
-	save the file
